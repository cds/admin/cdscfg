#!/bin/csh -ef 
#
#   stdsetup.csh - C-shell command to setup paths for CDS accounts
#
# * usage 
#  in ~/.tcshrc or ~/.cshrc 
#
#     source /ligo/cdscfg/stdsetup.csh 
#
#  Requires site-specific file /ligo/cdscfg/${site}/${ifo}/stdrc.csh
#
# ** ALWAYS FOUND AT /ligo/cdscfg
#
#  $Id: stdsetup.csh 1117 2014-05-27 17:42:05Z keith.thorne@LIGO.ORG $

# customize as needed
set cfgbase = "CDSCFG_CFG"

# What site are we at?
if ( -e ${cfgbase}/site/llo ) then
  setenv site llo
  setenv SITE LLO
  set ifolet = l
  set IFOHIGH = L
else if ( -e ${cfgbase}/site/lho ) then
  setenv site lho
  setenv SITE LHO
  set ifolet = h
  set IFOHIGH = H
else if ( -e $cfgbase/site/indigo ) then
  setenv site indigo
  setenv SITE INDIGO
  set ifolet = i
  set IFOHIGH = i
else if ( -e $cfgbase/site/kamioka ) then
  setenv site kamioka
  setenv SITE kamioka
  set ifolet = k
  set IFOHIGH = k
else if ( -e ${cfgbase}/site/mit ) then
  setenv site mit
  setenv SITE MIT
  set ifolet = m
  set IFOHIGH = M
else if ( -e ${cfgbase}/site/cit ) then
  setenv site cit
  setenv SITE CIT
  set ifolet = c
  set IFOHIGH = C
else if ( -e ${cfgbase}/site/geo ) then
  setenv site geo
  setenv SITE GEO
  set ifolet = g
  set IFOHIGH = G
else if ( -e ${cfgbase}/site/tst ) then
  setenv site tst
  setenv SITE TST
  set ifolet = x
  set IFOHIGH = X
else if ( -e ${cfgbase}/site/stn ) then
  setenv site stn
  setenv SITE STN
  set ifolet = s
  set IFOHIGH = S
else if ( -e ${cfgbase}/site/anu ) then
  setenv site anu
  setenv SITE ANU
  set ifolet = n
  set IFOHIGH = N
else if ( -e ${cfgbase}/site/uwa ) then
  setenv site uwa
  setenv SITE UWA
  set ifolet = u
  set IFOHIGH = u
else if ( -e ${cfgbase}/site/cardiff ) then
  setenv site cardiff
  setenv SITE CARDIFF
  set ifolet = w
  set IFOHIGH = w
else if ( -e ${cfgbase}/site/bham ) then
  setenv site bham
  setenv SITE BHAM
  set ifolet = b
  set IFOHIGH = B
else
  echo "I can't find a site file in ${cfgbase}/site."
  echo "I am setting your site to tst."
  setenv site tst
  setenv SITE TST
  set ifolet = x
  set IFOHIGH = X
endif

# now determine the IFO
if ( -e ${cfgbase}/ifo/${ifolet}0 ) then
     setenv IFONUM 0
else if ( -e ${cfgbase}/ifo/${ifolet}1 ) then
     setenv IFONUM 1
else if ( -e ${cfgbase}/ifo/${ifolet}2 ) then
     setenv IFONUM 2
else if ( -e ${cfgbase}/ifo/${ifolet}3 ) then
     setenv IFONUM 3
else if ( -e ${cfgbase}/ifo/${ifolet}4 ) then
     setenv IFONUM 4
else if ( -e ${cfgbase}/ifo/${ifolet}5 ) then
     setenv IFONUM 5
else if ( -e ${cfgbase}/ifo/${ifolet}6 ) then
     setenv IFONUM 6
else if ( -e ${cfgbase}/ifo/${ifolet}7 ) then
     setenv IFONUM 7
else if ( -e ${cfgbase}/ifo/${ifolet}8 ) then
     setenv IFONUM 8
else if ( -e ${cfgbase}/ifo/${ifolet}9 ) then
     setenv IFONUM 9
else
     echo "I can't find an ifo file in ${cfgbase}/ifo."
     echo "I am setting your site to ${ifolet}1."
     setenv IFONUM 1
endif 
setenv ifo ${ifolet}${IFONUM}
setenv IFO ${IFOHIGH}${IFONUM}

# Get the site,ifo-dependent setup
source ${cfgbase}/${site}/${ifo}/stdrc.csh
