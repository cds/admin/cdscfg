#!/bin/bash
#
# script cfg_ws.sh - config setup for workstation systems
#
#  - use /ligo/cdscfg for setup scripts
#  - apps are in /ligo/apps
if [ -n "${CDSCFG}" ] ; then
    cfgbase="${CDSCFG}"
    CFG_CUST="true"
    echo " Install CDS config at custom location: ${cfgbase}"
else
    cfgbase=/ligo/cdscfg
    CFG_CUST=
    echo " Install CDS config at default: ${cfgbase}"
fi
CDSCFG_CFG=${cfgbase}
#
if [ -f ${cfgbase}/stdsetup.sh ]; then
    CFG_NEW=
    echo "Found existing install - doing upgrade"
    source ${cfgbase}/stdsetup.sh
#
    read -p "Found site ["${site}"] - correct? (y/n)" SITE_ANS
    case $SITE_ANS in 
      y|Y) 
	    SITE_ARG=${site} ;;
      n|N)
        read -p "Enter LIGO site code (i.e. lho, llo, tst)" SITE_ARG ;;
      *)
        SITE_ARG=${site} ;;
    esac 
#
    read -p "Found ifo ["${ifo}"] - correct? (y/n)" IFO_ANS
    case $IFO_ANS in 
      y|Y) 
	IFO_ARG=${ifo} 
        IFO_IDX=${IFO_ARG:1:1}
	if [ ${MINIDX:+x} ] ; then
	    MINIDX_CFG=${MINIDX}
        else
            MINIDX_CFG=$IFO_IDX
        fi
	if [ ${MAXIDX:+x} ] ; then
	    MAXIDX_CFG=${MAXIDX}
        else
            MAXIDX_CFG=$IFO_IDX
        fi
	;;
      n|N)
        read -p "Enter 2-letter LIGO ifo code (i.e. h2, l1, x1)" IFO_ARG 
        IFO_IDX=${IFO_ARG:1:1}
	MINIDX_CFG=$IFO_IDX
	MAXIDX_CFG=$IFO_IDX
	;;
      *)
        IFO_ARG=${ifo} 
        IFO_IDX=${IFO_ARG:1:1}
	if [ ${MINIDX:+x} ] ; then
	    MINIDX_CFG=${MINIDX}
        else
            MINIDX_CFG=$IFO_IDX
        fi
	if [ ${MAXIDX:+x} ] ; then
	    MAXIDX_CFG=${MAXIFO}
        else
            MAXIDX_CFG=$IFO_IDX
        fi
	    ;;
    esac 
#    
    read -p "Found EPICS mask [""${EPICS_CA_ADDR_LIST}""] - correct? (y/n)" EPIC_ANS
    case $EPIC_ANS in 
      y|Y) 
	    EPIC_CFG="${EPICS_CA_ADDR_LIST}" ;;
      n|N)
        read -p "Enter EPICS network mask (i.e. 10.11.12.255, 10.144.0.255)" EPIC_CFG ;;
      *)
        EPIC_CFG="${EPICS_CA_ADDR_LIST}" ;;
    esac 
#           
    read -p "Found LIGO real-time broadcast mask [""${LIGO_RT_BCAST}""] - correct? (y/n)" BCAST_ANS
    case $BCAST_ANS in 
      y|Y) 
	    BCAST_CFG="${LIGO_RT_BCAST}" ;;
      n|N)
        read -p "Enter LIGO real-time broadcast mask (i.e. 10.110.140.255, 10.144.0.255)" BCAST_CFG ;;
      *)
        BCAST_CFG="${LIGO_RT_BCAST}" ;;
    esac 
#        
    read -p "Found NDS server ["${LIGONDSIP}"] - correct? (y/n)" NDS_ANS
    case $NDS_ANS in 
      y|Y) 
	NDS_CFG=${LIGONDSIP} ;;
      n|N) 
        read -p "Enter NDS server name (i.e. h2nds0, l1nds0)" NDS_CFG ;;
      *)
        NDS_CFG=${LIGONDSIP} ;;
    esac 
# 
    if [ "x:${LPDEST}" == "x" ]; then
        read -p "Enter printer name [$HOSTNAME]: " PRINTER_CFG    
        if [[ -z "$PRINTER_CFG" ]]; then
            PRINTER_CFG=$HOSTNAME
        fi
    else
        read -p "Found printer ["${LPDEST}"] - correct (y/n)" PR_ANS
        case $PR_ANS in 
          y|Y) 
	        PRINTER_CFG=${LPDEST} ;;
          n|N)
            read -p "Enter printer name [$HOSTNAME]: " PRINTER_CFG  
            if [[ -z "$PRINTER_CFG" ]]; then 
               PRINTER_CFG=$HOSTNAME
            fi
            ;;
          *)
            PRINTER_CFG=${LPDEST} ;;
        esac
    fi   
# 
    if [ "x:$APPSBASEDIR" == "x" ]; then
        read -p "Enter base apps folder [/ligo/apps]): " APPS_CFG
        if [[ -z "$APPS_CFG" ]]; then
           APPS_CFG="/ligo/apps"
        fi
    else
        read -p "Found base apps folder ["${APPSBASEDIR}"] - correct? (y/n)" APPS_ANS
        case $APPS_ANS in 
          y|Y) 
	        APPS_CFG=${APPSBASEDIR} ;;
          n|N)
            read -p "Enter base apps folder [/ligo/apps]: " APPS_CFG
            if [[ -z "$APPS_CFG" ]]; then
              APPS_CFG="/ligo/apps"
            fi
            ;;
          *)
            APPS_CFG=${APPSBASEDIR} ;;
        esac
    fi    
#
else
    CFG_NEW="true"
    echo "Must be a new install - please enter requested info"
    if [ $# -gt 0 ]; then
        SITE_ARG=$1
    else
        read -p "Enter LIGO site code (i.e. lho, llo, tst): " SITE_ARG
    fi
    if [ $# -gt 1 ]; then
        IFO_ARG=$2
    else
        read -p "Enter 2-letter LIGO ifo code (i.e. h2, l1, x1): " IFO_ARG
    fi

    IFO_IDX=${IFO_ARG:1:1}
    MINIDX_CFG=$IFO_IDX
    MAXIDX_CFG=$IFO_IDX

    if [ $# -gt 2 ]; then
        EPIC_CFG=$3
    else
        read -p "Enter EPICS network mask (i.e. 10.11.12.255, 10.144.0.255): " EPIC_CFG
    fi
    if [ $# -gt 3 ]; then
        BCAST_CFG=$4
    else
        read -p "Enter LIGO real-time broadcast mask (i.e. 10.110.140.255, 10.144.0.255): " BCAST_CFG
    fi
    if [ $# -gt 4 ]; then
        NDS_CFG=$5
    else
       read -p "Enter NDS server name (i.e. h2nds0, l1nds0): " NDS_CFG
    fi
    if [[ -z "$NDS_CFG" ]]; then
        NDS_CFG=${IFO_ARG}nds0
    fi
    if [ $# -gt 5 ]``; then
        PRINTER_CFG=$6
    else
        read -p "Enter default printer name [$HOSTNAME]: " PRINTER_CFG
    fi
    if [[ -z "$PRINTER_CFG" ]]; then
        PRINTER_CFG=$HOSTNAME
    fi
    if [ $# -gt 6 ]; then
        APPS_CFG=$7
    else
        read -p "Enter base apps folder [/ligo/apps]: " APPS_CFG
    fi
    if [[ -z "$APPS_CFG" ]]; then
        APPS_CFG="/ligo/apps"
    fi
fi

echo 'Install workstation config for '${SITE_ARG}'/'${IFO_ARG}' epics '${EPIC_CFG}' rt' ${BCAST_CFG}' nds '${NDS_CFG} '\n'
echo 'Installing for printer '${PRINTER_CFG} 'apps dir' ${APPS_CFG}
#
THIS_DIR=`pwd`
mkdir -p /ligo
mkdir -p ${cfgbase}
cd ${cfgbase}
mkdir -p ${SITE_ARG}
mkdir -p ${SITE_ARG}/${IFO_ARG}
# set for site/ifo
mkdir -p site
rm site/*
touch site/${SITE_ARG}
mkdir -p ifo
rm ifo/*
touch ifo/${IFO_ARG}
cd $THIS_DIR

# Install STDENV stuff
cp stdenv* ${cfgbase}
# copy stdenv templates (*.in) to non-templates
find ${cfgbase} -type f -name "*.in" | 
    sed "s/\.in$//" | 
    xargs -I% cp -fv %.in %
# add configs to stdenv setup
sed -i.bak s#CDSCFG_CFG#${CDSCFG_CFG}#g ${cfgbase}/stdenv*

# install site/STDENV stuff
cp site/stddir* ${cfgbase}/${SITE_ARG}
# copy Perl, Python templates (*.in) to non-templates
find ${cfgbase}/${SITE_ARG} -type f -name "*.in" | 
    sed "s/\.in$//" | 
    xargs -I% cp -fv %.in %
# add configs to Perl setup    
sed -i.bak s/MINIDX_CFG/${MINIDX_CFG}/g ${cfgbase}/${SITE_ARG}/*pl
sed -i.bak s/MAXIDX_CFG/${MAXIDX_CFG}/g ${cfgbase}/${SITE_ARG}/*pl
sed -i.bak s/EPIC_CFG/"${EPIC_CFG}"/g ${cfgbase}/${SITE_ARG}/*pl
sed -i.bak s/BCAST_CFG/"${BCAST_CFG}"/g ${cfgbase}/${SITE_ARG}/*pl
sed -i.bak s/NDS_CFG/${NDS_CFG}/g ${cfgbase}/${SITE_ARG}/*pl
sed -i.bak s/PRINTER_CFG/${PRINTER_CFG}/g ${cfgbase}/${SITE_ARG}/*pl
sed -i.bak s#APPS_CFG#${APPS_CFG}#g ${cfgbase}/${SITE_ARG}/*pl
# add configs to Python setup    
sed -i.bak s/MINIDX_CFG/${MINIDX_CFG}/g ${cfgbase}/${SITE_ARG}/*py
sed -i.bak s/MAXIDX_CFG/${MAXIDX_CFG}/g ${cfgbase}/${SITE_ARG}/*py
sed -i.bak s/EPIC_CFG/"${EPIC_CFG}"/g ${cfgbase}/${SITE_ARG}/*py
sed -i.bak s/BCAST_CFG/"${BCAST_CFG}"/g ${cfgbase}/${SITE_ARG}/*py
sed -i.bak s/NDS_CFG/${NDS_CFG}/g ${cfgbase}/${SITE_ARG}/*py
sed -i.bak s/PRINTER_CFG/${PRINTER_CFG}/g ${cfgbase}/${SITE_ARG}/*py
sed -i.bak s#APPS_CFG#${APPS_CFG}#g ${cfgbase}/${SITE_ARG}/*py

# copy script templates (*.in) to non-templates
cp site/ifo/stddir* ${cfgbase}/${SITE_ARG}/${IFO_ARG}
find ${cfgbase}/${SITE_ARG}/${IFO_ARG} -type f -name "stddir*.in" | 
    sed "s/\.in$//" | 
    xargs -I% cp -fv %.in %
# add configs to scripts
sed -i.bak s/MINIDX_CFG/${MINIDX_CFG}/g ${cfgbase}/${SITE_ARG}/${IFO_ARG}/*sh
sed -i.bak s/MAXIDX_CFG/${MAXIDX_CFG}/g ${cfgbase}/${SITE_ARG}/${IFO_ARG}/*sh
sed -i.bak s/EPIC_CFG/"${EPIC_CFG}"/g ${cfgbase}/${SITE_ARG}/${IFO_ARG}/*sh
sed -i.bak s/BCAST_CFG/"${BCAST_CFG}"/g ${cfgbase}/${SITE_ARG}/${IFO_ARG}/*sh
sed -i.bak s/NDS_CFG/${NDS_CFG}/g ${cfgbase}/${SITE_ARG}/${IFO_ARG}/*sh
sed -i.bak s/PRINTER_CFG/${PRINTER_CFG}/g ${cfgbase}/${SITE_ARG}/${IFO_ARG}/*sh
sed -i.bak s#APPS_CFG#${APPS_CFG}#g ${cfgbase}/${SITE_ARG}/${IFO_ARG}/*sh

# Install STDSETUP stuff
cp stdsetup* ${cfgbase}
# copy templates (*.in) to non-templates
find ${cfgbase} -type f -name "stdsetup*.in" | 
    sed "s/\.in$//" | 
    xargs -I% cp -fv %.in %
# add configs to STDSETUP setup
sed -i.bak s#CDSCFG_CFG#${CDSCFG_CFG}#g ${cfgbase}/stdsetup*

# Install site/ifo/STDSETUP stuff
cp site/ifo/stdrc* ${cfgbase}/${SITE_ARG}/${IFO_ARG}
# copy stdsetup templates (stdsetup,stdrc*.in) to non-templates
find ${cfgbase}/${SITE_ARG}/${IFO_ARG} -type f -name "stdrc*.in" | 
    sed "s/\.in$//" | 
    xargs -I% cp -fv %.in %
sed -i.bak s/MINIDX_CFG/${MINIDX_CFG}/g ${cfgbase}/${SITE_ARG}/${IFO_ARG}/*sh
sed -i.bak s/MAXIDX_CFG/${MAXIDX_CFG}/g ${cfgbase}/${SITE_ARG}/${IFO_ARG}/*sh
sed -i.bak s/EPIC_CFG/"${EPIC_CFG}"/g ${cfgbase}/${SITE_ARG}/${IFO_ARG}/*sh
sed -i.bak s/BCAST_CFG/"${BCAST_CFG}"/g ${cfgbase}/${SITE_ARG}/${IFO_ARG}/*sh
sed -i.bak s/NDS_CFG/${NDS_CFG}/g ${cfgbase}/${SITE_ARG}/${IFO_ARG}/*sh
sed -i.bak s/PRINTER_CFG/${PRINTER_CFG}/g ${cfgbase}/${SITE_ARG}/${IFO_ARG}/*sh
sed -i.bak s#APPS_CFG#${APPS_CFG}#g ${cfgbase}/${SITE_ARG}/${IFO_ARG}/*sh
#
# remove installer files
# make things executable
rm ${cfgbase}/*.in
rm ${cfgbase}/*.bak
chmod 755 ${cfgbase}/*.sh
chmod 755 ${cfgbase}/*.tcl
chmod 755 ${cfgbase}/*.csh
chmod 755 ${cfgbase}/*.pl
chmod 755 ${cfgbase}/*.py
rm ${cfgbase}/${SITE_ARG}/*.in
rm ${cfgbase}/${SITE_ARG}/*.bak
chmod 755 ${cfgbase}/${SITE_ARG}/*.pl
chmod 755 ${cfgbase}/${SITE_ARG}/*.py
rm ${cfgbase}/${SITE_ARG}/${IFO_ARG}/*.in
rm ${cfgbase}/${SITE_ARG}/${IFO_ARG}/*.bak
chmod 755 ${cfgbase}/${SITE_ARG}/${IFO_ARG}/*.sh
chmod 755 ${cfgbase}/${SITE_ARG}/${IFO_ARG}/*.csh
#
# add to account startup
insert_stdsetup() {
        setup_file="$1"
        install_to="$2"
        setup_text=`cat $setup_file`
        bshtmp=$(mktemp)

        added=0
        while IFS=$'\n' read -r line ; do
                printf "$line\n" >> "$bshtmp"
                [ $added = 0 ] && grep -q '^#' <<<"$line" && echo "$setup_text" >>"$bshtmp" && added=1
        done < "$install_to"
        mv -f "$bshtmp" "$install_to"
}   
if [ $CFG_NEW ]; then
   cd $THIS_DIR
   if [ $CFG_CUST ]; then
      cp bashrc_ws_custom.in bashrc_ws 
      cp cshrc_ws_custom.in cshrc_ws 
   else
      cp bashrc_ws.in bashrc_ws 
      cp cshrc_ws.in cshrc_ws 
   fi
   sed -i.bak s#CDSCFG_CFG#${CDSCFG_CFG}#g *hrc_ws
   if [ -e ~/.bashrc ]; then   
       if grep -q -i stdsetup ~/.bashrc ; then
           echo "stdsetup already added to bashrc"
       else
           echo "Add stdsetup to bashrc"
           insert_stdsetup bashrc_ws ~/.bashrc
       fi
   else
       echo "create .bashrc"
       cp bashrc_ws ~/.bashrc
   fi
   if [ -e ~/.cshrc ]; then 
       if grep -q -i stdsetup ~/.cshrc; then
           echo "stdsetup already added to cshrc"
       else
           echo "Add stdsetup to cshrc"
           insert_stdsetup cshrc_ws ~/.cshrc
       fi
   else
       echo "create .cshrc" 
       cp cshrc_ws ~/.cshrc
   fi
fi

