# Source this file to access METAIO
setenv METAIO_PREFIX "/ligo/apps/METAIO-8.2"
set prefix="$METAIO_PREFIX"
set exec_prefix="$METAIO_PREFIX"
#
setenv PATH "${exec_prefix}/bin:${PATH}"
if ( $?LD_LIBRARY_PATH ) then
        setenv LD_LIBRARY_PATH "${exec_prefix}/lib:${LD_LIBRARY_PATH}"
else
        setenv LD_LIBRARY_PATH "${exec_prefix}/lib"
endif
if ( $?DYLD_LIBRARY_PATH ) then
        setenv DYLD_LIBRARY_PATH "${exec_prefix}/lib:${DYLD_LIBRARY_PATH}"
else
        setenv DYLD_LIBRARY_PATH "${exec_prefix}/lib"
endif
if ( $?PKG_CONFIG_PATH ) then
        setenv PKG_CONFIG_PATH "${exec_prefix}/lib/pkgconfig:${PKG_CONFIG_PATH}"
else
        setenv PKG_CONFIG_PATH "${exec_prefix}/lib/pkgconfig"
endif
unset prefix
unset exec_prefix
