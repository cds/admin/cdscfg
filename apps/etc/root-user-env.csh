# Source this file to access ROOT
setenv ROOTSYS "/ligo/apps/root-5.26.00c"
set prefix="/ligo/apps/root-5.26.00c"
set exec_prefix="/ligo/apps/root-5.26.00c"

setenv PATH "${exec_prefix}/bin:${PATH}"
if ( $?LD_LIBRARY_PATH ) then
        setenv LD_LIBRARY_PATH "${exec_prefix}/lib/root:${LD_LIBRARY_PATH}"
else
        setenv LD_LIBRARY_PATH "${exec_prefix}/lib/root"
endif
if ( $?DYLD_LIBRARY_PATH ) then
        setenv DYLD_LIBRARY_PATH "${exec_prefix}/lib/root:${DYLD_LIBRARY_PATH}"
else
        setenv DYLD_LIBRARY_PATH "${exec_prefix}/lib/root"
endif
if ( $?PYTHONPATH ) then
        setenv PYTHONPATH "${exec_prefix}/lib/root:${PYTHONPATH}"
else
        setenv PYTHONPATH "${exec_prefix}/lib/root"
endif
unset prefix
unset exec_prefix
