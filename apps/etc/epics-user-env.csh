# Setup EPICS
#  First determine EPICS_HOST_ARCH
#  - kernelname should be Linux or SunOS
set kernelname=`uname -s`
#  - proctype should be x86_64, x86 or sparc
set proctype=`uname -m`
if ($kernelname == Linux) then
    if ($proctype == x86_64) then
        setenv EPICS_HOST_ARCH linux-x86_64
    else
        setenv EPICS_HOST_ARCH linux-x86
    endif
else if ($kernelname == SunOS) then
    if ($proctype == sparc) then
        setenv EPICS_HOST_ARCH solaris-sparc
    else
        setenv EPICS_HOST_ARCH solaris-x86
    endif
else
   echo 'unknown kernel '$kernelname
endif
# define paths for EPICS
set epicstop=/ligo/apps/epics-3.14.10
setenv EPICS_BASE ${epicstop}/base
setenv EPICS_BASEDIR ${epicstop}/base
setenv EPICS_MODULES ${epicstop}/modules
setenv EPICS_SEQ ${EPICS_MODULES}$/seq
setenv EPICS_EXTENSIONS ${epicstop}/extensions
# only add if not already in path
setenv EPICSBIN ${EPICS_BASE}/bin/${EPICS_HOST_ARCH} 
echo ${PATH} | egrep -i "${EPICS_BASE}/bin/${EPICS_HOST_ARCH}" >&/dev/null
if ($status != 0) then
   setenv PATH ${EPICS_BASE}/bin/${EPICS_HOST_ARCH}:${PATH}
endif
echo ${PATH} | egrep -i "${EPICS_SEQ}/bin/${EPICS_HOST_ARCH}" >&/dev/null
if ($status != 0) then
   setenv PATH ${EPICS_SEQ}/bin/${EPICS_HOST_ARCH}:${PATH}
endif
echo ${PATH} | egrep -i "${EPICS_EXTENSIONS}/bin/${EPICS_HOST_ARCH}" >&/dev/null
if ($status != 0) then
   setenv PATH ${EPICS_EXTENSIONS}/bin/${EPICS_HOST_ARCH}:${PATH}
endif
setenv EPICSLIB ${EPICS_BASE}/lib/${EPICS_HOST_ARCH}
echo ${LD_LIBRARY_PATH} | egrep -i "${EPICS_BASE}/lib/${EPICS_HOST_ARCH}" >&/dev/null
if ($status != 0) then
   setenv LD_LIBRARY_PATH ${EPICS_BASE}/lib/${EPICS_HOST_ARCH}:${LD_LIBRARY_PATH}
endif
echo ${LD_LIBRARY_PATH} | egrep -i "${EPICS_SEQ}/lib/${EPICS_HOST_ARCH}" >&/dev/null
if ($status != 0) then
   setenv LD_LIBRARY_PATH ${EPICS_SEQ}/lib/${EPICS_HOST_ARCH}:${LD_LIBRARY_PATH}
endif
echo ${LD_LIBRARY_PATH} | egrep -i "${EPICS_EXTENSIONS}/lib/${EPICS_HOST_ARCH}" >&/dev/null
if ($status != 0) then
   setenv LD_LIBRARY_PATH ${EPICS_EXTENSIONS}/lib/${EPICS_HOST_ARCH}:${LD_LIBRARY_PATH}
endif
