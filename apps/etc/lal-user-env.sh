# Source this file to access LAL
LAL_PREFIX="/ligo/apps/lal-6.4.1"
export LAL_PREFIX
prefix="/ligo/apps/lal-6.4.1"
exec_prefix="/ligo/apps/lal-6.4.1"
#
prepend()
{
#  prepend - add new item to path if not already there
    newpath=$1
    oldpath=$2
    compath=$3
    if [ -z "${compath}" ]; then
        export ${oldpath}=${newpath}
    else
        echo ${compath} | egrep -i ${newpath} >&/dev/null
        if [ "$?" -ne 0 ]; then
            export ${oldpath}=${newpath}:${compath}
        fi
    fi
}
prepend "${exec_prefix}/bin" "PATH" "${PATH}" 
prepend "${exec_prefix}/lib" "LD_LIBRARY_PATH" "${LD_LIBRARY_PATH}"
prepend "${exec_prefix}/lib" "DYLD_LIBRARY_PATH" "${DYLD_LIBRARY_PATH}"
prepend "${exec_prefix}/lib/pkgconfig" "PKG_CONFIG_PATH" "${PKG_CONFIG_PATH}"
unset prefix
unset exec_prefix

