# Source this file to access ROOT
ROOTSYS="/ligo/apps/root-5.26.00c"
export ROOTSYS
prefix="/ligo/apps/root-5.26.00c"
exec_prefix="/ligo/apps/root-5.26.00c"
#
prepend()
{
#  prepend - add new item to path if not already there
    newpath=$1
    oldpath=$2
    compath=$3
    if [ -z "${compath}" ]; then
        export ${oldpath}=${newpath}
    else
        echo ${compath} | egrep -i ${newpath} >&/dev/null
        if [ "$?" -ne 0 ]; then
            export ${oldpath}=${newpath}:${compath}
        fi
    fi
}
prepend "${exec_prefix}/bin" "PATH" "${PATH}" 
prepend "${exec_prefix}/lib/root" "LD_LIBRARY_PATH" "${LD_LIBRARY_PATH}"
prepend "${exec_prefix}/lib/root" "DYLD_LIBRARY_PATH" "${DYLD_LIBRARY_PATH}"
prepend "${exec_prefix}/lib/root" "PYTHONPATH" "${PYTHONPATH}"
unset prefix
unset exec_prefix

