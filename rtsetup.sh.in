#!/bin/bash --noprofile
#
#   rtsetup.sh - Bourne-again shell setup for real-time CDS
#
# * usage is *
#  in ~/.bashrc or ~/.profile
#
#     source /opt/cdscfg/rtsetup.sh
#
#  Requires site-specific file /opt/cdscfg/${site}/${ifo}/rtrc.sh
#
# ** usually found at /opt/cdscfg/ - OVERRIDE with CDSCFG variable
#
#  $Id: rtsetup.sh 1117 2014-05-27 17:42:05Z keith.thorne@LIGO.ORG $

if [ -n "${CDSCFG}" -a -d "${CDSCFG}" ] ; then
    cfgbase="${CDSCFG}"
else
    cfgbase=CDSCFG_CFG
fi

# What site are we at?
#
if [ -f ${cfgbase}/site/llo ]; then
    site=llo
    SITE=LLO
    ifolet=l
    IFOHIGH=L
elif [ -f ${cfgbase}/site/lho ]; then
    site=lho
    SITE=LHO
    ifolet=h
    IFOHIGH=H
elif [ -f ${cfgbase}/site/indigo ]; then
    site=indigo
    SITE=INDIGO
    ifolet=i
    IFOHIGH=i
elif [ -f ${cfgbase}/site/kamioka ]; then
    site=kamioka
    SITE=KAMIOKA
    ifolet=k
    IFOHIGH=k
elif [ -f ${cfgbase}/site/cit ]; then
    site=cit
    SITE=CIT
    ifolet=c
    IFOHIGH=C
elif [ -f ${cfgbase}/site/mit ]; then
    site=mit
    SITE=MIT
    ifolet=m
    IFOHIGH=M
elif [ -f ${cfgbase}/site/geo ]; then
    site=geo
    SITE=GEO
    ifolet=g
    IFOHIGH=G
elif [ -f ${cfgbase}/site/tst ]; then
    site=tst
    SITE=TST
    ifolet=x
    IFOHIGH=X
elif [ -f ${cfgbase}/site/stn ]; then
    site=stn
    SITE=STN
    ifolet=s
    IFOHIGH=S
elif [ -f ${cfgbase}/site/anu ]; then
    site=anu
    SITE=ANU
    ifolet=n
    IFOHIGH=N
elif [ -f ${cfgbase}/site/uwa ]; then
    site=uwa
    SITE=UWA
    ifolet=u
    IFOHIGH=U
elif [ -f ${cfgbase}/site/cardiff ]; then
    site=cardiff
    SITE=CARDIFF
    ifolet=w
    IFOHIGH=W
elif [ -f ${cfgbase}/site/bham ]; then
    site=bham
    SITE=BHAM
    ifolet=b
    IFOHIGH=B
else 
    echo "I can't find a site file in ${cfgbase}/site."
    echo "I am setting your site to tst."
    site=tst
    SITE=TST
    ifolet=x
    IFOHIGH=X
fi
export site
export SITE

# now get ifo
if [ -f ${cfgbase}/ifo/${ifolet}0 ]; then
	IFONUM=0
elif [ -f ${cfgbase}/ifo/${ifolet}1 ]; then
	IFONUM=1
elif [ -f ${cfgbase}/ifo/${ifolet}2 ]; then
	IFONUM=2
elif [ -f ${cfgbase}/ifo/${ifolet}3 ]; then
	IFONUM=3
elif [ -f ${cfgbase}/ifo/${ifolet}4 ]; then
	IFONUM=4
elif [ -f ${cfgbase}/ifo/${ifolet}5 ]; then
	IFONUM=5
elif [ -f ${cfgbase}/ifo/${ifolet}6 ]; then
	IFONUM=6
elif [ -f ${cfgbase}/ifo/${ifolet}7 ]; then
	IFONUM=7
elif [ -f ${cfgbase}/ifo/${ifolet}8 ]; then
	IFONUM=8
elif [ -f ${cfgbase}/ifo/${ifolet}9 ]; then
	IFONUM=9
else 
	echo "I can't find an ifo file in ${cfgbase}/ifo."
	echo "I am setting your site to ${ifolet}1."
	IFONUM=1
fi
ifo=${ifolet}${IFONUM}
IFO=${IFOHIGH}${IFONUM}
export IFONUM 
export ifo
export IFO

#  Get site, ifo, platform specific paths
source ${cfgbase}/${site}/${ifo}/rtrc.sh
