# Standard Directories (python)
# Used by Standard Environment
#  - These are the LLO ones

import os
import sys

###
def LINUX_DIR():
	global SYSBIN, SYSLIB, SYSPERL, SYSPYTHON
	global EPICSDIR, GDSDIR

# determine the operating system
	if os.path.isfile("/etc/gentoo-release"):
		kernrel = os.uname()[2]
		kernmajor = kernrel.split('.')[0]
		kernminor = kernrel.split('.')[1]
		if kernmajor == "2":
			OS_ARCH = "linux-x86_64"      
			APPSROOT = appsdir
		else:
			OS_ARCH = "gentoo" + kernmajor + "-" + kernminor
			APPSROOT = os.path.join(appsdir,OS_ARCH) 
	else:
		osname = os.popen('/usr/bin/awk -F = \'$1 == "ID" {gsub(/"/, "", $2); print $2}\' <  "/etc/os-release"')
		distname = osname.readline().rstrip()
		osname.close()
		osrel = os.popen('/usr/bin/awk -F = \'$1 == "VERSION_ID" {gsub(/"/, "", $2); print $2}\' <  "/etc/os-release"'))
		distrel = osrel.readline().rstrip()
		osrel.close()
		distnum = distrel.split('.')[0]
		if distname == "ubuntu":
			OS_ARCH = "ubuntu" + distnum
		elif distname == "scientific":
			OS_ARCH = "sl" + distnum
		elif distname == "centos":
			OS_ARCH = "centos" + distnum
		elif distname == "debian":
			OS_ARCH = "debian" + distnum
		elif distname == "rocky":
			OS_ARCH = "rocky" + distnum
		else:
			OS_ARCH = "linux-x86_64"
		APPSROOT = os.path.join(appsdir,OS_ARCH)

# SYSBIN should be standard paths
	SYSBIN = "/usr/bin:/usr/sbin:/bin:/sbin"

# SYSLIB as well
	SYSLIB = "/usr/lib:/lib"

# add local if exists
	if os.path.isdir("/usr/local"):
		SYSBIN = ":".join(["/usr/local/bin",SYSBIN])
		SYSLIB = ":".join(["/usr/local/lib",SYSLIB])

# Get info on processor
# - proctype should be sparc, i386 or x86_64
	proctype = os.uname()[4]

# add 64-bit system libraries
	EPICSPKGPATH = "/usr/lib/epics"
	if proctype == "x86_64":
# these tend to be Debian systems
		if os.path.isdir("/lib/x86_64-linux-gnu"):
			  EPICSPKGPATH = "/usr/lib/epics"
			  SYSLIB = ":".join(["/lib/x86_64-linux-gnu",SYSLIB])
			  if os.path.isdir("/usr/lib/x86_64-linux-gnu"):
			    SYSLIB = ":".join(["/usr/lib/x86_64-linux-gnu",SYSLIB])
		else:
# these tend to be Redhat, Gentoo
			  EPICSPKGPATH = "/usr/lib64/epics"
			  if os.path.isdir("/lib64"):
			    SYSLIB = ":".join(["/lib64",SYSLIB])
			    if os.path.isdir("/usr/lib64"):
			      SYSLIB = ":".join(["/usr/lib64",SYSLIB])

# add location of Perl modules like CaTools, TdsTools
	modtop = os.path.join(APPSROOT,"/perlmodules")
	if os.path.isdir(modtop):
			SYSPERL = modtop
	else:
			SYSPERL = None
 
# get the Python version
	PYTHONREV = ".".join([str(sys.version_info[0]),str(sys.version_info[1])])
	PYTHONSUB = "python" + PYTHONREV  

# basic system Python path
	SYSPYTHON = os.path.join("/usr/lib",PYTHONSUB)
	distpython = os.path.join("/usr/lib",PYTHONSUB,"dist-packages")
	if os.path.isdir(distpython):
		 SYSPYTHON = ":".join([distpython,SYSPYTHON])
	sitepython = os.path.join("/usr/lib",PYTHONSUB,"site-packages")
	if os.path.isdir(sitepython): 
		 SYSPYTHON = ":".join([sitepython,SYSPYTHON])

	# set processor-dependent directory
	if proctype == "x86_64":
		EPICS_HOST_ARCH = "linux-x86_64"
	else:
		EPICS_HOST_ARCH = "linux-x86"

	# ** Setup EPICS **
	epicstop = os.path.join(APPSROOT, "epics")
	EPICSBIN = None
	EPICSLIB = None
	EPICSPERL = None

	# check for appsroot install
	if os.path.isdir(epicstop):
		EPICSDIR = epicstop
		EPICS_BASE = os.path.join(epicstop, "base")
		EPICS_EXTENSIONS = os.path.join(epicstop, "extensions")
		EPICSBIN = ":".join([os.path.join(EPICS_BASE, "bin", EPICS_HOST_ARCH), os.path.join(EPICS_EXTENSIONS, "bin", EPICS_HOST_ARCH)])
		EPICSLIB = ":".join([os.path.join(EPICS_BASE, "lib", EPICS_HOST_ARCH), os.path.join(EPICS_EXTENSIONS, "lib", EPICS_HOST_ARCH)])   
		# add location of EPICS Perl modules 
		EPICSPERL = os.path.join(EPICS_BASE, "lib", "perl")
		# add pyepics
		PYEPICS = os.path.join(epicstop,"pyext","pyepics","lib",PYTHONSUB,"site-packages")
		if os.path.isdir(PYEPICS):
			sys.path.append(PYEPICS)
			SYSPYTHON = ":".join([PYEPICS,SYSPYTHON])
		# add pcaspy
		PCASPY = os.path.join(epicstop,"pyext","pcaspy","lib",PYTHONSUB,"site-packages")
		if os.path.isdir(PCASPY):
			sys.path.append(PCASPY)
			SYSPYTHON = ":".join([PCASPY,SYSPYTHON])

	# otherwise, check for CDS package install
	else:
		epicstop = EPICSPKGPATH  
		if os.path.isdir(epicstop):
			EPICSDIR = epicstop
			EPICSBIN = os.path.join(epicstop, "bin", EPICS_HOST_ARCH)
			EPICSLIB = os.path.join(epicstop, "lib", EPICS_HOST_ARCH)
			# add location of EPICS Perl modules 
			EPICSPERL = os.path.join(epicstop, "lib", "perl")

	if EPICSBIN is not None:
		SYSBIN = ":".join([EPICSBIN,SYSBIN])
	if EPICSLIB is not None:
		SYSLIB = ":".join([EPICSLIB,SYSLIB])
	if EPICSPERL is not None:
		if SYSPERL is not None:
			SYSPERL = ":".join([SYSPERL,EPICSPERL]) 
		else:
			SYSPERL = EPICSPERL 

	# ** setup ldas-tools **
	ldastop = os.path.join(APPSROOT, "ldas-tools")
	# check for appsroot install
	if os.path.isdir(ldastop):
		PYLDAS = os.path.join(ldastop, "lib", PYTHONSUB, "site-packages")
		if os.path.isdir(PYLDAS):
			sys.path.append(PYLDAS)
			SYSPYTHON = ":".join([PYLDAS,SYSPYTHON])
 
	# ** setup GDS **
	gdstop = os.path.join(APPSROOT, "gds")
	# check for appsroot install
	GDSDIR = None 
	GDSBIN = None 
	GDSLIB = None 
	if os.path.isdir(gdstop):
		GDSDIR = gdstop
		GDSBIN = os.path.join(GDSDIR, "bin")
		GDSLIB = os.path.join(GDSDIR, "lib")
	else:
		if os.path.isfile("/usr/bin/diag"):
			GDSDIR = "/usr"
	
	if GDSBIN:
		SYSBIN = ":".join([GDSBIN,SYSBIN])
	if GDSLIB:
		SYSLIB = ":".join([GDSLIB,SYSLIB])
 
	# ** setup Root **
	roottop = os.path.join(APPSROOT, "root")
	if os.path.isdir(roottop):
		ROOTLIB = os.path.join(roottop, "lib", "root")
		SYSLIB = ":".join([ROOTLIB,SYSLIB])

	# HACK KT 2/1/2013 add location of tconvert
	ligotoolstop = os.path.join(APPSROOT, "ligotools")
	if os.path.isdir(ligotoolstop):
		 LIGOTOOLSBIN = os.path.join(ligotoolstop, "bin")
		 SYSBIN = ":".join([LIGOTOOLSBIN,SYSBIN])
		 LIGOTOOLSLIB = os.path.join(ligotoolstop, "lib")
		 SYSLIB = ":".join([LIGOTOOLSLIB,SYSLIB])

	# ** setup nds2 **
	nds2top = os.path.join(APPSROOT, "nds2-client")
	# check for appsroot install
	if os.path.isdir(nds2top):
		NDS2BIN = os.path.join(nds2top, "bin")
		SYSBIN = ":".join([NDS2BIN,SYSBIN])
		NDS2LIB = os.path.join(nds2top, "lib", PYTHONSUB, "site-packages")
		if os.path.isdir(NDS2LIB):
			sys.path.append(NDS2LIB)
			SYSPYTHON = ":".join([NDS2LIB,SYSPYTHON])

	# ** setup MATLAB **
	matlabtop = os.path.join(APPSROOT, "matlab")
	if os.path.isdir(matlabtop):
		MATDIR = matlabtop

	# add location of camera modules
	cameratop = os.path.join(APPSROOT, "camera")
	if os.path.isdir(cameratop):
		CAMERALIB = os.path.join(cameratop,"lib")
		sys.path.append(CAMERALIB)
		SYSPYTHON = ":".join([CAMERALIB,SYSPYTHON])

	# add location of cdsutils
	cdsutilstop = os.path.join(APPSROOT,"cdsutils")
	if os.path.isdir(cdsutilstop):
		CDSUTILSBIN = os.path.join(cdsutilstop, "bin")
		SYSBIN = ":".join([CDSUTILSBIN,SYSBIN])
		CDSUTILSLIB = os.path.join(cdsutilstop, "lib",PYTHONSUB,"site-packages")
		if os.path.isdir(CDSUTILSLIB): 
			sys.path.append(CDSUTILSLIB)
			SYSPYTHON = ":".join([CDSUTILSLIB,SYSPYTHON])

	# add location of gpstime
	gpstimetop = os.path.join(APPSROOT,"gpstime")
	if os.path.isdir(gpstimetop):
		GPSTIMEBIN = os.path.join(gpstimetop, "bin")
		SYSBIN = ":".join([GPSTIMEBIN,SYSBIN])
		GPSTIMELIB = os.path.join(gpstimetop, "lib",PYTHONSUB,"site-packages")
		if os.path.isdir(GPSTIMELIB): 
			sys.path.append(GPSTIMELIB)
			SYSPYTHON = ":".join([GPSTIMELIB,SYSPYTHON])

	# add location of guardian
	guardiantop = os.path.join(APPSROOT,"guardian")
	if os.path.isdir(guardiantop):
		GUARDIANBIN = os.path.join(guardiantop, "bin")
		SYSBIN = ":".join([GUARDIANBIN,SYSBIN])
		GUARDIANLIB = os.path.join(guardiantop, "lib",PYTHONSUB,"site-packages")
		if os.path.isdir(GUARDIANLIB): 
			sys.path.append(GUARDIANLIB)
			SYSPYTHON = ":".join([GUARDIANLIB,SYSPYTHON])

#  we will handle Linux and several solaris variants 
# my $location of scripts
sitescriptdir = os.path.join(cdscfg, site)

# set min, max IFO ids
MINIFO = MINIDX_CFG
MAXIFO = MAXIDX_CFG

# bring LIGONDSIP to top level
LIGONDSIP = "NDS_CFG"
os.environ['LIGONDSIP'] = LIGONDSIP
NDSSERVER = "NDS_CFG:8088"
os.environ['NDSSERVER'] = NDSSERVER

EPICS_CA_ADDR_LIST = "EPIC_CFG"
EPICS_CA_AUTO_ADDR_LIST = "NO"
os.environ['EPICS_CA_ADDR_LIST'] = EPICS_CA_ADDR_LIST
os.environ['EPICS_CA_AUTO_ADDR_LIST'] = EPICS_CA_AUTO_ADDR_LIST

# set real-time awgtpman broadcast mask
LIGO_RT_BCAST = "BCAST_CFG"
os.environ['LIGO_RT_BCAST'] = LIGO_RT_BCAST

# add location of front-end user applications
if os.getenv('PRIVATE_USERAPPS') is not None:
		USERAPPS = os.getenv('PRIVATE_USERAPPS')
else:
		USERAPPS = os.path.join(os.path.sep, 'opt', 'rtcds', 'userapps', 'release')

# set top-level directory for workstation apps
appsdir = "APPS_CFG"

# Get info on OS and processor
#  - kernelname should be Linux, SunOS or Darwin
kernelname = os.uname()[0]

# IF Linux then
#     call Linux setup 
# ELSE if Solaris
#     call Solaris setup
# ELSE if Mac OS X
#     call OS X setup
# ENDIF
if kernelname == "Linux":
		LINUX_DIR()
else:
		print("unsupported platform " + kernelname)

