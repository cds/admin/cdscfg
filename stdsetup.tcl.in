# Tcl code written by Peter Shawhan, April 24, 2008.
# Works by creating a bash shell, executing the stdenv.sh script,
# and then grabbing all the environment variables that result.
# Therefore, this Tcl script hopefully will not need any maintenance!

# Usage note: In your Tcl code, do 'source /cvs/cds/site/stdenv.tcl'.
# That runs the stdenv.sh in the default way, with no arguments.
# Then, if you need to, you can re-run stdenv.sh with an argument by
# calling the INIT_ENV procedure with that argument.

# Updated 24 Mar 2009 KT to use env instead of printenv

#-- Create a procedure to do this operation
proc INIT_ENV { args } {

    if { ! [file exists "/ligo/cdscfg/stdsetup.sh"] } {
	puts stderr "Error: /ligo/cdscfg/stdsetup.sh is missing"
	exit 1
    }
    if [ catch {
	set newenv \
                [exec /bin/bash -c "source /ligo/cdscfg/stdsetup.sh $args; env"]
    } errmsg ] {
	puts stderr "Error executing bash to set environment variables"
#	puts stderr $errmsg
	exit 2
    }

    global env

    #-- Now loop over lines of output from 'printenv' and set OUR environment
    #-- variables accordingly (except for a few special ones)
    foreach { line var value } \
            [ regexp -all -inline -line {^([^=]*)=(.*)$} $newenv ] {
	   if { $var == "SHLVL" || $var == "_" } {
	      #-- Do nothing
	   } else {
         set env($var) $value
	   }
    }

    return
}

#-- Now call the procedure
eval INIT_ENV $argv
