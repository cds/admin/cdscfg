# Package stdenv3 (Python3)
#   - sets everything automagically for LIGO EPICS scripts
#
#  Suggested usage is

#  #!/usr/bin/env python3
#
#  import sys
#  sys.path.append('/ligo/cdscfg')
#  import stdenv3 as cds;
#
#  cds.INIT_ENV(**)

#  Where ** is an IFO index or label 0,1,2,L0,H2
#  or nothing for L1 or H1.
#
#  usually in /ligo/cdscfg - use CDSCFG to override
#
#  Requires site-specific file /ligo/cdscfg/${site}/${ifo}/stddir3.py
#
#  && sets PATH, LD_LIBRARY_PATH environment variables

#  $Id: stdenv.py 1044 2012-11-28 22:28:21Z michael.thomas@LIGO.ORG $

import datetime
import time
import os

if os.getenv('CDSCFG') is not None:
	cdscfg=os.getenv('CDSCFG');
else:
	cdscfg='CDSCFG_CFG'

# get site
if os.path.isfile(os.path.join(cdscfg, 'site', 'llo')):
	site = 'llo'
	SITE = 'LLO'
	ifolet = 'l'
	IFOHIGH = 'L'
elif os.path.isfile(os.path.join(cdscfg, 'site', 'lho')):
	site = 'lho'
	SITE = 'LHO'
	ifolet = 'h'
	IFOHIGH = 'H'
elif os.path.isfile(os.path.join(cdscfg, 'site', 'indigo')):
	site = 'indigo'
	SITE = 'INDIGO'
	ifolet = 'i'
	IFOHIGH = 'i'
elif os.path.isfile(os.path.join(cdscfg, 'site', 'kamioka')):
	site = 'kamioka'
	SITE = 'KAMIOKA'
	ifolet = 'k'
	IFOHIGH = 'k'
elif os.path.isfile(os.path.join(cdscfg, 'site', 'mit')):
	site = 'mit'
	SITE = 'MIT'
	ifolet = 'm'
	IFOHIGH = 'M'
elif os.path.isfile(os.path.join(cdscfg, 'site', 'cit')):
	site = 'cit'
	SITE = 'CIT'
	ifolet = 'c'
	IFOHIGH = 'C'
elif os.path.isfile(os.path.join(cdscfg, 'site', 'geo')):
	site = 'geo'
	SITE = 'GEO'
	ifolet = 'g'
	IFOHIGH = 'G'
elif os.path.isfile(os.path.join(cdscfg, 'site', 'tst')):
	site = 'tst'
	SITE = 'TST'
	ifolet = 'x'
	IFOHIGH = 'X'
elif os.path.isfile(os.path.join(cdscfg, 'site', 'stn')):
	site = 'stn'
	SITE = 'STN'
	ifolet = 's'
	IFOHIGH = 'S'
elif os.path.isfile(os.path.join(cdscfg, 'site', 'anu')):
	site = 'anu'
	SITE = 'ANU'
	ifolet = 'n'
	IFOHIGH = 'N'
elif os.path.isfile(os.path.join(cdscfg, 'site', 'uwa')):
	site = 'uwa'
	SITE = 'UWA'
	ifolet = 'u'
	IFOHIGH = 'U'
elif os.path.isfile(os.path.join(cdscfg, 'site', 'cardiff')):
	site = 'cardiff'
	SITE = 'CARDIFF'
	ifolet = 'w'
	IFOHIGH = 'W'
else:
	print("I can't find a site file in " + cdscfg + "/site.\n")
	print("  I am setting your site to tst.\n")
	site = 'tst'
	SITE = 'TST'
	ifolet = 'x'
	IFOHIGH = 'X'

os.environ["site"] = site
os.environ["SITE"] = SITE
os.environ["ifolet"] = ifolet
os.environ["IFOHIGH"] = IFOHIGH

# Load the site-specific setup script

# Get site-specific directories
exec(compile(open(os.path.join(cdscfg, site, 'stddir3.py'), "rb").read(), os.path.join(cdscfg, site, 'stddir3.py'), 'exec'))

# NOW define the path and libraries (unless you are in conda)
if "CONDA_SHLVL" in os.environ:
	CONDALVL = os.environ.get('CONDA_SHLVL')
	if CONDALVL == 0:
		os.environ['PATH'] = SYSBIN
		os.environ['LD_LIBRARY_PATH'] = SYSLIB
		os.environ['PERL5LIB'] = SYSPERL
		os.environ['PYTHONPATH'] = SYSPYTHON
else:
	os.environ['PATH'] = SYSBIN
	os.environ['LD_LIBRARY_PATH'] = SYSLIB
	os.environ['PERL5LIB'] = SYSPERL
	os.environ['PYTHONPATH'] = SYSPYTHON

#########################################
def INIT_ENV(ifoIndex=None):
	global ASC, GDS, HPI, IOO, ISI, LSC, OMC, PEM, PSL, SEI, SUS, TCS
	global RTCDSDIR, SCRIPTDIR, MEDMDIR, TARGETDIR, CHANSDIR
	global SITEDIR, SITESCRIPTDIR, CDSDIR
	global IFONUM, ifo, IFO
	global DATELINE

	IFONUM = None

	# Determine the IFO number
	if ifoIndex == None:
	# If nothing input, check for ifo files
	
		if os.path.isfile(cdscfg + '/ifo/' + ifolet + '0'):
			IFONUM = 0
		elif os.path.isfile(cdscfg + '/ifo/' + ifolet + '1'):
			IFONUM = 1
		elif os.path.isfile(cdscfg + '/ifo/' + ifolet + '2'):
			IFONUM = 2
		elif os.path.isfile(cdscfg + '/ifo/' + ifolet + '3'):
			IFONUM = 3
		elif os.path.isfile(cdscfg + '/ifo/' + ifolet + '4'):
			IFONUM = 4
		elif os.path.isfile(cdscfg + '/ifo/' + ifolet + '5'):
			IFONUM = 5
		else:
			print("Did not find " + cdscfg + "/ifo file")
			print("  Setting IFO to " + ifolet + "1")
			IFONUM = 1
	else:
		ifoIndex = str(ifoIndex).upper()
		for index in range(0, 5):
			if ifoIndex == str(index) or ifoIndex == IFOHIGH + str(index):
				IFONUM = index
				if IFONUM == None:
					print("Invalid IFO " + ifoIndex + " specified for " + SITE)
					print("  Setting IFO to " + ifolet + "1")
					IFONUM = 1

	ifo = ifolet + str(IFONUM)
	IFO = IFOHIGH + str(IFONUM)
	os.environ["ifo"] = ifo
	os.environ["IFO"] = IFO

  # directories that depend on IFO and SITE
	SITEDIR = os.path.join(os.path.sep, "ligo", "cds", site)
	SITESCRIPTDIR = os.path.join(SITEDIR, "scripts")
	CDSDIR = os.path.join(SITEDIR, ifo)
	os.environ["SITEDIR"] = SITEDIR
	os.environ["SITESCRIPTDIR"] = SITESCRIPTDIR
	os.environ["CDSDIR"] = CDSDIR

	# directories for IFO work
	RTCDSDIR = os.path.join(os.path.sep, "opt", "rtcds", site, ifo)
	SCRIPTDIR = os.path.join(RTCDSDIR, "scripts")
	MEDMDIR = os.path.join(RTCDSDIR, "medm")
	TARGETDIR = os.path.join(RTCDSDIR, "target")
	CHANSDIR = os.path.join(RTCDSDIR, "chans")
	os.environ["RTCDSDIR"] = RTCDSDIR
	os.environ["SCRIPTDIR"] = SCRIPTDIR
	os.environ["MEDMDIR"] = MEDMDIR
	os.environ["TARGETDIR"] = TARGETDIR
	os.environ["CHANSDIR"] = CHANSDIR

	# Set Subsystem Shortcuts
	ALS = ":".join([IFO, "ALS"])
	ASC = ":".join([IFO, "ASC"])
	CAL = ":".join([IFO, "CAL"])
	GDS = ":".join([IFO, "GDS"])
	HPI = ":".join([IFO, "HPI"])
	INJ = ":".join([IFO, "INJ"])
	IOO = ":".join([IFO, "IOO"])
	ISC = ":".join([IFO, "ISC"])
	ISI = ":".join([IFO, "ISI"])
	LSC = ":".join([IFO, "LSC"])
	OMC = ":".join([IFO, "OMC"])
	PEM = ":".join([IFO, "PEM"])
	PSL = ":".join([IFO, "PSL"])
	SEI = ":".join([IFO, "SEI"])
	SUS = ":".join([IFO, "SUS"])
	SYS = ":".join([IFO, "SYS"])
	TCS = ":".join([IFO, "TCS"])
	VE = ":".join([IFO, "VE"])

	now = datetime.datetime.now()
	DATELINE = "%02d%02d%02d_%02d%02d%02d" % (now.year%100, now.month, now.day, now.hour, now.minute, now.second)

