#!/usr/bin/env bash --noprofile 
#
#   stdenv.sh - Bourne-again shell commands to define site-specific paths
#		Sets directories and paths
#		    CDSDIR - top-level site-specific CDS directory
#		    ADMINSCRIPT - admin script directory
#		    GENSCRIPT - general script directory
#		    MEDMDIR - MEDM screen top-level
#		    TARGETDIR - 'target' directors
#		    CHANSDIR - 'chans' directory
#		    MINPATH - minimal path 
#		    APPSDIR - Applications directory
#		    MATDIR - MATLAB directory
#		Subsystem Shortcuts
#	IFO, ASC,IOO,ISI,ISS,LSC,OMC,PEM,PSL,SEI,SUS,TCS
#		Dateline - current date string
#
# NOTE: To ensure that your script avoid excessive setup
#    invoke the Bourne-again shell with profiles disabled
# * usage is *
#  #!/usr/bin/env bash --noprofile
#  source /ligo/cdscfg/stdenv.sh 
#		-or-
#  source /ligo/cdscfg/stdenv.sh <ifoparm> 
#               -or- 
#  export IFOARG=<ifoparm>
#  source /ligo/cdscfg/stdenv.sh 
#  
#   where <ifoparm> is an ifo prefix (H0,H1,H2,l0,L1,G1,1,2..)
#
#  Requires site-specific file /ligo/cdscfg/${site}/${ifo}/stddir.sh
#
# ** usually found at /ligo/cdscfg  OVERRIDE with CDSCFG variable
#  i.e. #!/usr/bin/env bash --noprofile
#
#  $Id: stdenv.sh 1130 2014-07-14 20:07:10Z keith.thorne@LIGO.ORG $

if [ -n "${CDSCFG}" -a -d "${CDSCFG}" ] ; then
    cfgbase="${CDSCFG}"
else
    cfgbase=CDSCFG_CFG
fi

# What site are we at?
#
if [ -f ${cfgbase}/site/llo ]; then
    site=llo
    SITE=LLO
    ifolet=l
    IFOHIGH=L
elif [ -f ${cfgbase}/site/lho ]; then
    site=lho
    SITE=LHO
    ifolet=h
    IFOHIGH=H
elif [ -f ${cfgbase}/site/cit ]; then
    site=cit
    SITE=CIT
    ifolet=c
    IFOHIGH=C
elif [ -f ${cfgbase}/site/indigo ]; then
    site=indigo
    SITE=INDIGO
    ifolet=i
    IFOHIGH=i
elif [ -f ${cfgbase}/site/kamioka ]; then
    site=kamioka
    SITE=KAMIOKA
    ifolet=k
    IFOHIGH=K
elif [ -f ${cfgbase}/site/mit ]; then
    site=mit
    SITE=MIT
    ifolet=m
    IFOHIGH=M
elif [ -f ${cfgbase}/site/geo ]; then
    site=geo
    SITE=GEO
    ifolet=g
    IFOHIGH=G
elif [ -f ${cfgbase}/site/tst ]; then
    site=tst
    SITE=TST
    ifolet=x
    IFOHIGH=X
elif [ -f ${cfgbase}/site/stn ]; then
    site=stn
    SITE=STN
    ifolet=s
    IFOHIGH=S
elif [ -f ${cfgbase}/site/anu ]; then
    site=anu
    SITE=ANU
    ifolet=n
    IFOHIGH=N
elif [ -f ${cfgbase}/site/uwa ]; then
    site=uwa
    SITE=UWA
    ifolet=u
    IFOHIGH=U
elif [ -f ${cfgbase}/site/cardiff ]; then
    site=cardiff
    SITE=CARDIFF
    ifolet=w
    IFOHIGH=W
elif [ -f ${cfgbase}/site/bham ]; then
    site=bham
    SITE=BHAM
    ifolet=b
    IFOHIGH=B
else 
    echo "I can't find a site file in ${cfgbase}/site."
    echo "I am setting your site to tst."
    site=tst
    SITE=TST
    ifolet=x
    IFOHIGH=X
fi
export site
export SITE

# Were we passed IFO information 
# on the command line?
if [ $# -gt 0 ]; then
   IFOPAR=$1
else
  if [ "$IFOARG" ]; then
    IFOPAR=${IFOARG}
  fi
fi

if [ ! "$IFOPAR" ]; then
# now get ifo
  if [ -f ${cfgbase}/ifo/${ifolet}0 ]; then
	IFONUM=0
  elif [ -f ${cfgbase}/ifo/${ifolet}1 ]; then
	IFONUM=1
  elif [ -f ${cfgbase}/ifo/${ifolet}2 ]; then
	IFONUM=2
  elif [ -f ${cfgbase}/ifo/${ifolet}3 ]; then
	IFONUM=3
  elif [ -f ${cfgbase}/ifo/${ifolet}4 ]; then
	IFONUM=4
  elif [ -f ${cfgbase}/ifo/${ifolet}5 ]; then
	IFONUM=5
  elif [ -f ${cfgbase}/ifo/${ifolet}6 ]; then
	IFONUM=6
  elif [ -f ${cfgbase}/ifo/${ifolet}7 ]; then
	IFONUM=7
  elif [ -f ${cfgbase}/ifo/${ifolet}8 ]; then
	IFONUM=8
  elif [ -f ${cfgbase}/ifo/${ifolet}9 ]; then
	IFONUM=9
  else 
	echo "I can't find an ifo file in ${cfgbase}/ifo."
	echo "I am setting your site to ${ifolet}1."
    IFONUM=1
  fi
else
  if [ "$IFOPAR" = "0" ]; then
    IFONUM=0
  elif [ "$IFOPAR" = "1" ]; then
    IFONUM=1
  elif [ "$IFOPAR" = "2" ]; then
    IFONUM=2
  elif [ "$IFOPAR" = "3" ]; then
    IFONUM=3
  elif [ "$IFOPAR" = "4" ]; then
    IFONUM=4
  elif [ "$IFOPAR" = "5" ]; then
    IFONUM=5
  elif [ "$IFOPAR" = "6" ]; then
    IFONUM=6
  elif [ "$IFOPAR" = "7" ]; then
    IFONUM=7
  elif [ "$IFOPAR" = "8" ]; then
    IFONUM=8
  elif [ "$IFOPAR" = "9" ]; then
    IFONUM=9
  elif [ "$IFOPAR" = "${IFOHIGH}0" ]; then
    IFONUM=0
  elif [ "$IFOPAR" = "${IFOHIGH}1" ]; then
    IFONUM=1
  elif [ "$IFOPAR" = "${IFOHIGH}2" ]; then
    IFONUM=2
  elif [ "$IFOPAR" = "${IFOHIGH}3" ]; then
    IFONUM=3
  elif [ "$IFOPAR" = "${IFOHIGH}4" ]; then
    IFONUM=4
  elif [ "$IFOPAR" = "${IFOHIGH}5" ]; then
    IFONUM=5
  elif [ "$IFOPAR" = "${IFOHIGH}6" ]; then
    IFONUM=6
  elif [ "$IFOPAR" = "${IFOHIGH}7" ]; then
    IFONUM=7
  elif [ "$IFOPAR" = "${IFOHIGH}8" ]; then
    IFONUM=8
  elif [ "$IFOPAR" = "${IFOHIGH}9" ]; then
    IFONUM=9
  elif [ "$IFOPAR" = "${ifolet}0" ]; then
    IFONUM=0
  elif [ "$IFOPAR" = "${ifolet}1" ]; then
    IFONUM=1
  elif [ "$IFOPAR" = "${ifolet}2" ]; then
    IFONUM=2
  elif [ "$IFOPAR" = "${ifolet}3" ]; then
    IFONUM=3
  elif [ "$IFOPAR" = "${ifolet}4" ]; then
    IFONUM=4
  elif [ "$IFOPAR" = "${ifolet}5" ]; then
    IFONUM=5
  elif [ "$IFOPAR" = "${ifolet}6" ]; then
    IFONUM=6
  elif [ "$IFOPAR" = "${ifolet}7" ]; then
    IFONUM=7
  elif [ "$IFOPAR" = "${ifolet}8" ]; then
    IFONUM=8
  elif [ "$IFOPAR" = "${ifolet}9" ]; then
    IFONUM=9
  else
    echo "Invalid IFO $IFOPAR specified for ${SITE}"
    echo "  Setting IFO to ${ifolet}1"
    IFONUM=1
  fi
fi
ifo=${ifolet}${IFONUM}
IFO=${IFOHIGH}${IFONUM}
export IFONUM
export ifo
export IFO

#  Get site, platform specific paths
. ${cfgbase}/${site}/${ifo}/stddir.sh

# NOW force PATH, LD_LIBRARY_PATH, PYTHONPATH, PERL5LIB to 
PATH=${SYSBIN}
LD_LIBRARY_PATH=${SYSLIB}
PYTHONPATH=${SYSPYTHON}
PERL5LIB=${SYSPERL}
export PATH
export LD_LIBRARY_PATH
export PYTHONPATH
export PERL5LIB

# CLEAR any LD_PRELOAD
unset LD_PRELOAD

# directories that depend on IFO and SITE
SITEDIR=/ligo/cds/${site}
export SITEDIR
SITESCRIPTDIR=${SITEDIR}/scripts
export SITESCRIPTDIR
CDSDIR=/ligo/cds/${site}/${ifo}
export CDSDIR

# directories for IFO work
RTCDSDIR=/opt/rtcds/${site}/${ifo}
export RTCDSDIR
SCRIPTDIR=${RTCDSDIR}/scripts
export SCRIPTDIR
MEDMDIR=${RTCDSDIR}/medm
export MEDMDIR
TARGETDIR=${RTCDSDIR}/target
export TARGETDIR
CHANSDIR=${RTCDSDIR}/chans
export CHANSDIR

# Set Subsystem Shortcuts
ALS=${IFO}:ALS
export ALS
ASC=${IFO}:ASC
export ASC
CAL=${IFO}:CAL
export CAL
GDS=${IFO}:GDS
export GDS
HPI=${IFO}:HPI
export HPI
INJ=${IFO}:INJ
export INJ
IOO=${IFO}:IOO
export IOO
ISC=${IFO}:ISC
export ISC
ISI=${IFO}:ISI
export ISI
LSC=${IFO}:LSC
export LSC
OMC=${IFO}:OMC
export OMC
PEM=${IFO}:PEM
export PEM
PSL=${IFO}:PSL
export PSL
SEI=${IFO}:SEI
export SEI
SUS=${IFO}:SUS
export SUS
SYS=${IFO}:SYS
export SYS
TCS=${IFO}:TCS
export TCS
VE=${IFO}:VE
export VE

# current date string (at the end)
DATELINE=`date +%y%m%d_%H%M%S`
export DATELINE
