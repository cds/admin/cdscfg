#!/bin/bash --noprofile 

source stdenv.sh $1

echo "site is $site, number $IFONUM, ifo $ifo, date $DATELINE"
echo "Updated path is:"
echo $PATH
echo "Updated Perl module path is:"
echo $PERL5LIB
echo "Updated Python library path is:"
echo $PYTHONPATH
echo "Where is EPICS caget?"
which caget
echo "Where is GDS diag?"
which diag
echo "ASC is $ASC, LSC is $LSC"

