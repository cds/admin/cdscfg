# Source this file to access METAIO
METAIO_PREFIX="/opt/rtapps/metaio-8.2"
export METAIO_PREFIX
prefix="$METAIO_PREFIX"
exec_prefix="$METAIO_PREFIX"
#
prepend()
{
#  prepend - add new item to path if not already there
    newpath=$1
    oldpath=$2
    compath=$3
    if [ -z "${compath}" ]; then
        export ${oldpath}=${newpath}
    else
        echo ${compath} | egrep -i ${newpath} >&/dev/null
        if [ "$?" -ne 0 ]; then
            export ${oldpath}=${newpath}:${compath}
        fi
    fi
}
prepend "${exec_prefix}/bin" "PATH" "${PATH}" 
prepend "${exec_prefix}/lib" "LD_LIBRARY_PATH" "${LD_LIBRARY_PATH}"
prepend "${exec_prefix}/lib" "DYLD_LIBRARY_PATH" "${DYLD_LIBRARY_PATH}"
prepend "${exec_prefix}/lib/pkgconfig" "PKG_CONFIG_PATH" "${PKG_CONFIG_PATH}"
unset prefix
unset exec_prefix
