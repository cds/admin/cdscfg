# Setup EPICS
#  First determine EPICS_HOST_ARCH
#  - kernelname should be Linux or SunOS
kernelname=`uname -s`
#  - proctype should be x86_64, x86 or sparc
proctype=`uname -m`
if [ "$kernelname" == "Linux" ]; then
    if [ "$proctype" == "x86_64" ]; then
        export EPICS_HOST_ARCH=linux-x86_64
    else
        export EPICS_HOST_ARCH=linux-x86
    fi
elif [ "$kernelname" == "SunOS" ]; then
    if [ "$proctype" == "sparc" ]; then
        export EPICS_HOST_ARCH=solaris-sparc
    else
        export EPICS_HOST_ARCH=solaris-x86
    fi
else
   echo 'unknown kernel '$kernelname
fi

# * Now setup up paths
epicstop=/opt/rtapps/epics-3.14.10
export EPICS_BASE=${epicstop}/base
export EPICS_SEQ=${epicstop}/modules/sncseq
export EPICS_EXTENSIONS=${epicstop}/extensions
prepend()
{
#  prepend - add new item to path if not already there
    newpath=$1
    oldpath=$2
    compath=$3
    if [ -z "${compath}" ]; then
        export ${oldpath}=${newpath}
    else
        echo ${compath} | egrep -i ${newpath} >&/dev/null
        if [ "$?" -ne 0 ]; then
            export ${oldpath}=${newpath}:${compath}
        fi
    fi
}
prepend "${exec_prefix}/bin" "PATH" "${PATH}" 
prepend "${exec_prefix}/lib" "LD_LIBRARY_PATH" "${LD_LIBRARY_PATH}"
prepend "${exec_prefix}/lib" "DYLD_LIBRARY_PATH" "${DYLD_LIBRARY_PATH}"
prepend "${exec_prefix}/lib/pkgconfig" "PKG_CONFIG_PATH" "${PKG_CONFIG_PATH}"

prepend "${EPICS_BASE}/bin/${EPICS_HOST_ARCH}" "PATH" "${PATH}"
prepend "${EPICS_SEQ}/bin/${EPICS_HOST_ARCH}" "PATH" "${PATH}"
prepend "${EPICS_EXTENSIONS}/bin/${EPICS_HOST_ARCH}" "PATH" "${PATH}"
prepend "${EPICS_BASE}/lib/${EPICS_HOST_ARCH}" "LD_LIBRARY_PATH" "${LD_LIBRARY_PATH}"
prepend "${EPICS_SEQ}/lib/${EPICS_HOST_ARCH}" "LD_LIBRARY_PATH" "${LD_LIBRARY_PATH}"
prepend "${EPICS_EXTENSIONS}/lib/${EPICS_HOST_ARCH}" "LD_LIBRARY_PATH" "${LD_LIBRARY_PATH}"
