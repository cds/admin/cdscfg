# Source this file to access LIBFRAME
LIBFRAME_PREFIX="/opt/rtapps/libframe-8.11"
export LIBFRAME_PREFIX
prefix="/opt/rtapps/libframe-8.11"
exec_prefix="/opt/rtapps/libframe-8.11"
#
prepend()
{
#  prepend - add new item to path if not already there
    newpath=$1
    oldpath=$2
    compath=$3
    if [ -z "${compath}" ]; then
        export ${oldpath}=${newpath}
    else
        echo ${compath} | egrep -i ${newpath} >&/dev/null
        if [ "$?" -ne 0 ]; then
            export ${oldpath}=${newpath}:${compath}
        fi
    fi
}
prepend "${exec_prefix}/bin" "PATH" "${PATH}" 
prepend "${exec_prefix}/lib" "LD_LIBRARY_PATH" "${LD_LIBRARY_PATH}"
prepend "${exec_prefix}/lib" "DYLD_LIBRARY_PATH" "${DYLD_LIBRARY_PATH}"
prepend "${exec_prefix}/lib/pkgconfig" "PKG_CONFIG_PATH" "${PKG_CONFIG_PATH}"
unset prefix
unset exec_prefix
