# Source this file to access framecpp
prefix="/opt/rtapps/framecpp-1.18.2"
exec_prefix="/opt/rtapps/framecpp-1.18.2/linux-x86_64"
PATH="${exec_prefix}/bin:${PATH}"
LD_LIBRARY_PATH="${exec_prefix}/lib:${LD_LIBRARY_PATH}"
DYLD_LIBRARY_PATH="${exec_prefix}/lib:${DYLD_LIBRARY_PATH}"
PKG_CONFIG_PATH="${exec_prefix}/lib/pkgconfig:${PKG_CONFIG_PATH}"
export PATH LD_LIBRARY_PATH DYLD_LIBRARY_PATH PKG_CONFIG_PATH
unset prefix
unset exec_prefix

