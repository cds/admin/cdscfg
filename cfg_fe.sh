#!/bin/bash
#
# script cfg_fe.sh - config setup for front-end systems
#
#  - use /opt/cdscfg for setup scripts
#  - apps are in /opt/rtapps
if [ -n "${CDSCFG}" ] ; then
    cfgbase="${CDSCFG}"
    echo " Install FE CDS config at custom location: ${cfgbase}"
else
    cfgbase=/opt/cdscfg
    echo " Install FE CDS config at default: ${cfgbase}"
fi
CDSCFG_CFG=${cfgbase}
#
if [ -n "${CDSCFG_NOINTERFACE}" ] ; then
    echo " Running $0 with no interface.  Variables must be defined in the environment. "    
    if [ -f ${cfgbase}/rtsetup.sh ]; then
        CFG_NEW=
    else
	CFG_NEW='true'
    fi	
else
    if [ -f ${cfgbase}/rtsetup.sh ]; then
        CFG_NEW=
        echo "Found existing install - doing upgrade"
        source ${cfgbase}/rtsetup.sh
    #
        read -p "Found site ["${site}"] - correct? (y/n)" SITE_ANS
        case $SITE_ANS in 
          y|Y) 
    	SITE_ARG=${site} ;;
          n|N)
            read -p "Enter LIGO site code (i.e. lho, llo, tst):" SITE_ARG ;;
          *)
            SITE_ARG=${site} ;;
        esac 
    #
        read -p "Found ifo ["${ifo}"] - correct? (y/n)" IFO_ANS
        case $IFO_ANS in 
          y|Y) 
    	IFO_ARG=${ifo} 
            IFO_IDX=${IFO_ARG:1:1}
    	if [ ${MINIDX:+x} ] ; then
    	    MINIDX_CFG=${MINIDX}
            else
                MINIDX_CFG=$IFO_IDX
            fi
    	if [ ${MAXIDX:+x} ] ; then
    	    MAXIDX_CFG=${MAXIDX}
            else
                MAXIDX_CFG=$IFO_IDX
            fi
    	 ;;
          n|N)
            read -p "Enter 2-letter LIGO ifo code (i.e. h2, l1, x1)" IFO_ARG 
            IFO_IDX=${IFO_ARG:1:1}
    	MINIDX_CFG=$IFO_IDX
    	MAXIDX_CFG=$IFO_IDX
    	;;
          *)
            IFO_ARG=${ifo} 
            IFO_IDX=${IFO_ARG:1:1}
    	if [ ${MINIDX:+x} ] ; then
    	    MINIDX_CFG=${MINIDX}
            else
                MINIDX_CFG=$IFO_IDX
            fi
    	if [ ${MAXIDX:+x} ] ; then
    	    MAXIDX_CFG=${MAXIDX}
            else
                MAXIDX_CFG=$IFO_IDX
            fi
    	;;
        esac 
    #    
        read -p "Found EPICS mask [""${EPICS_CA_ADDR_LIST}""] - correct (y/n)" EPIC_ANS
        case $EPIC_ANS in 
          y|Y) 
    	    EPIC_CFG="${EPICS_CA_ADDR_LIST}" ;;
          n|N)
            read -p "Enter EPICS network mask (i.e. 10.11.12.255, 10.144.0.255):" EPIC_CFG ;;
          *)
            EPIC_CFG="${EPICS_CA_ADDR_LIST}" ;;
        esac 
    #    
        read -p "Found NDS server ["${LIGONDSIP}"] - correct? (y/n)" NDS_ANS
        case $NDS_ANS in 
          y|Y) 
    	    NDS_CFG=${LIGONDSIP} ;;
          n|N)
            read -p "Enter NDS server name ["${HOSTNAME}"]:" NDS_CFG ;;
          *)
            NDS_CFG=${LIGONDSIP} ;;
        esac 
    # 
        if [ "x:$LPDEST" == "x" ]; then
            read -p "Enter printer name ["$HOSTNAME"]: " PRINTER_CFG    
            if [[ -z  "$PRINTER_CFG" ]]; then
                PRINTER_CFG=$HOSTNAME
            fi
        else
            read -p "Found printer ["${LPDEST}"] - correct? (y/n)" PR_ANS
            case $PR_ANS in 
              y|Y) 
    	        PRINTER_CFG=${LPDEST} ;;
              n|N)
                read -p "Enter printer name ["${LPDEST}"]: " PRINTER_CFG  
    	    if [[ -z "${PRINTER_CFG}" ]]; then 
                   PRINTER_CFG=${LPDEST}
                fi
                ;;
              *)
                PRINTER_CFG=${LPDEST} ;;
            esac
        fi   
    # 
        if [ "x:$APPSBASEDIR" == "x" ]; then
            read -p "Enter base apps folder [/opt/rtapps]): " APPS_CFG
            if [[ -z "$APPS_CFG" ]]; then
               APPS_CFG="/opt/rtapps"
            fi
        else
            read -p "Found base apps folder ["${APPSBASEDIR}"] - correct? (y/n)" APPS_ANS
            case $APPS_ANS in 
              y|Y) 
    	        APPS_CFG=${APPSBASEDIR} ;;
              n|N)
                read -p "Enter base apps folder [/opt/rtapps]: " APPS_CFG
                if [[ -z "$APPS_CFG" ]]; then
                  APPS_CFG="/opt/rtapps"
                fi
                ;;
              *)
                APPS_CFG=${APPSBASEDIR} ;;
            esac
        fi    
    #
    else
        CFG_NEW="true"
        echo "Must be a new install - please enter requested info"
        if [ $# -gt 0 ]; then
            SITE_ARG=$1
        else
            read -p "Enter LIGO site code (i.e. lho, llo, tst): " SITE_ARG
        fi
        if [ $# -gt 1 ]; then
            IFO_ARG=$2
        else
            read -p "Enter 2-letter LIGO ifo code (i.e. h2, l1, x1): " IFO_ARG
        fi
        IFO_IDX=${IFO_ARG:1:1}
    	MINIDX_CFG=$IFO_IDX
    	MAXIDX_CFG=$IFO_IDX
        if [ $# -gt 2 ]; then
            EPIC_CFG=$3
        else
            read -p "Enter EPICS network mask (i.e. 10.11.12.255, 10.144.0.255): " EPIC_CFG
        fi
        if [ $# -gt 3 ]; then
            NDS_CFG=$4
        else
           read -p "Enter NDS server name (i.e. h2nds0, l1nds0): " NDS_CFG
        fi
        if [[ -z "$NDS_CFG" ]]; then
            NDS_CFG=$HOSTNAME
        fi
        if [ $# -gt 4 ]; then
            PRINTER_CFG=$5
        else
            read -p "Enter default printer name [$HOSTNAME]: " PRINTER_CFG
        fi
        if [[ -z "$PRINTER_CFG" ]]; then
            PRINTER_CFG=$HOSTNAME
        fi
        if [ $# -gt 5 ]; then
            APPS_CFG=$6
        else
            read -p "Enter base apps folder [/opt/rtapps]: " APPS_CFG
        fi
        if [[ -z "$APPS_CFG" ]]; then
            APPS_CFG="/opt/rtapps"
        fi
    fi
fi
echo 'Install front-end config for dir '${cfgbase} 'ifo' ${SITE_ARG}'/'${IFO_ARG}' net '${EPIC_CFG}' nds '${NDS_CFG}
echo 'Installing for printer '${PRINTER_CFG} 'apps dir' ${APPS_CFG}
#
THIS_DIR=`pwd`
mkdir -p ${cfgbase}
cd ${cfgbase}
mkdir -p ${SITE_ARG}
mkdir -p ${SITE_ARG}/${IFO_ARG}
# set for site/ifo
mkdir -p site
rm site/*
touch site/${SITE_ARG}
mkdir -p ifo
rm ifo/*
touch ifo/${IFO_ARG}
cd $THIS_DIR

# Install STDENV stuff
cp stdenv* ${cfgbase}
# copy templates (*.in) to non-templates
find ${cfgbase} -type f -name "stdenv*.in" | 
    sed "s/\.in$//" | 
    xargs -I% cp -fv %.in %
# add configs to stdenv setup
sed -i.bak s#CDSCFG_CFG#${CDSCFG_CFG}#g ${cfgbase}/stdenv*

# Install site/STDENV stuff
cp site/stddir* ${cfgbase}/${SITE_ARG}
# copy Perl, Python templates (*.in) to non-templates
find ${cfgbase}/${SITE_ARG} -type f -name "*.in" | 
    sed "s/\.in$//" | 
    xargs -I% cp -fv %.in %
# add configs to Perl setup    
sed -i.bak s/MINIDX_CFG/${MINIDX_CFG}/g ${cfgbase}/${SITE_ARG}/*pl
sed -i.bak s/MAXIDX_CFG/${MAXIDX_CFG}/g ${cfgbase}/${SITE_ARG}/*pl
sed -i.bak s/EPIC_CFG/"${EPIC_CFG}"/g ${cfgbase}/${SITE_ARG}/*pl
# on front-end system EPICS and AWG Broadcast masks will be the same
sed -i.bak s/BCAST_CFG/"${EPIC_CFG}"/g ${cfgbase}/${SITE_ARG}/*pl
sed -i.bak s/NDS_CFG/${NDS_CFG}/g ${cfgbase}/${SITE_ARG}/*pl
sed -i.bak s/PRINTER_CFG/${PRINTER_CFG}/g ${cfgbase}/${SITE_ARG}/*pl
sed -i.bak s#APPS_CFG#${APPS_CFG}#g ${cfgbase}/${SITE_ARG}/*pl
# add configs to Python setup    
sed -i.bak s/MINIDX_CFG/${MINIDX_CFG}/g ${cfgbase}/${SITE_ARG}/*py
sed -i.bak s/MAXIDX_CFG/${MAXIDX_CFG}/g ${cfgbase}/${SITE_ARG}/*py
sed -i.bak s/EPIC_CFG/"${EPIC_CFG}"/g ${cfgbase}/${SITE_ARG}/*py
sed -i.bak s/BCAST_CFG/"${EPIC_CFG}"/g ${cfgbase}/${SITE_ARG}/*py
sed -i.bak s/NDS_CFG/${NDS_CFG}/g ${cfgbase}/${SITE_ARG}/*py
sed -i.bak s/PRINTER_CFG/${PRINTER_CFG}/g ${cfgbase}/${SITE_ARG}/*py
sed -i.bak s#APPS_CFG#${APPS_CFG}#g ${cfgbase}/${SITE_ARG}/*py

# copy stdenv script templates (stddir*.in) to non-templates
cp site/ifo/stddir* ${cfgbase}/${SITE_ARG}/${IFO_ARG}
find ${cfgbase}/${SITE_ARG}/${IFO_ARG} -type f -name "stddir*.in" | 
    sed "s/\.in$//" | 
    xargs -I% cp -fv %.in %
# add configs to scripts
sed -i.bak s/MINIDX_CFG/${MINIDX_CFG}/g ${cfgbase}/${SITE_ARG}/${IFO_ARG}/*sh
sed -i.bak s/MAXIDX_CFG/${MAXIDX_CFG}/g ${cfgbase}/${SITE_ARG}/${IFO_ARG}/*sh
sed -i.bak s/EPIC_CFG/"${EPIC_CFG}"/g ${cfgbase}/${SITE_ARG}/${IFO_ARG}/*sh
sed -i.bak s/BCAST_CFG/"${EPIC_CFG}"/g ${cfgbase}/${SITE_ARG}/${IFO_ARG}/*sh
sed -i.bak s/NDS_CFG/${NDS_CFG}/g ${cfgbase}/${SITE_ARG}/${IFO_ARG}/*sh
sed -i.bak s/PRINTER_CFG/${PRINTER_CFG}/g ${cfgbase}/${SITE_ARG}/${IFO_ARG}/*sh
sed -i.bak s#APPS_CFG#${APPS_CFG}#g ${cfgbase}/${SITE_ARG}/${IFO_ARG}/*sh

# Install RTSETUP stuff
cp rtsetup* ${cfgbase}
# copy templates (*.in) to non-templates
find ${cfgbase} -type f -name "rtsetup*.in" | 
    sed "s/\.in$//" | 
    xargs -I% cp -fv %.in %
# add configs to RTSETUP setup
sed -i.bak s#CDSCFG_CFG#${CDSCFG_CFG}#g ${cfgbase}/rtsetup*

# Install site/ifo setup stuff
cp site/ifo/rtrc* ${cfgbase}/${SITE_ARG}/${IFO_ARG}
cp site/ifo/stdrc* ${cfgbase}/${SITE_ARG}/${IFO_ARG}
# copy rtrc templates (rtrc*.in) to non-templates
find ${cfgbase}/${SITE_ARG}/${IFO_ARG} -type f -name "rtrc*.in" | 
    sed "s/\.in$//" | 
    xargs -I% cp -fv %.in %
# copy stdrc templates (stdrc*.in) to non-templates
find ${cfgbase}/${SITE_ARG}/${IFO_ARG} -type f -name "stdrc*.in" | 
    sed "s/\.in$//" | 
    xargs -I% cp -fv %.in %
# add configs to scripts
sed -i.bak s/MINIDX_CFG/${MINIDX_CFG}/g ${cfgbase}/${SITE_ARG}/${IFO_ARG}/*sh
sed -i.bak s/MAXIDX_CFG/${MAXIDX_CFG}/g ${cfgbase}/${SITE_ARG}/${IFO_ARG}/*sh
sed -i.bak s/EPIC_CFG/"${EPIC_CFG}"/g ${cfgbase}/${SITE_ARG}/${IFO_ARG}/*sh
sed -i.bak s/BCAST_CFG/"${EPIC_CFG}"/g ${cfgbase}/${SITE_ARG}/${IFO_ARG}/*sh
sed -i.bak s/NDS_CFG/${NDS_CFG}/g ${cfgbase}/${SITE_ARG}/${IFO_ARG}/*sh
sed -i.bak s/PRINTER_CFG/${PRINTER_CFG}/g ${cfgbase}/${SITE_ARG}/${IFO_ARG}/*sh
sed -i.bak s#APPS_CFG#${APPS_CFG}#g ${cfgbase}/${SITE_ARG}/${IFO_ARG}/*sh
#
# remove installer files
# make things executable
rm ${cfgbase}/*.in
rm ${cfgbase}/*.bak
chmod 755 ${cfgbase}/*.sh
chmod 755 ${cfgbase}/*.tcl
chmod 755 ${cfgbase}/*.csh
chmod 755 ${cfgbase}/*.pl
chmod 755 ${cfgbase}/*.py
rm ${cfgbase}/${SITE_ARG}/*.in
rm ${cfgbase}/${SITE_ARG}/*.bak
chmod 755 ${cfgbase}/${SITE_ARG}/*.pl
chmod 755 ${cfgbase}/${SITE_ARG}/*.py
rm ${cfgbase}/${SITE_ARG}/${IFO_ARG}/*.in
rm ${cfgbase}/${SITE_ARG}/${IFO_ARG}/*.bak
chmod 755 ${cfgbase}/${SITE_ARG}/${IFO_ARG}/*.sh
chmod 755 ${cfgbase}/${SITE_ARG}/${IFO_ARG}/*.csh
#
# add to account startup
insert_stdsetup() {
        setup_file="$1"
        install_to="$2"
        setup_text=`cat $setup_file`
        bshtmp=$(mktemp)

        added=0
        while IFS=$'\n' read -r line ; do
                printf "$line\n" >> "$bshtmp"
                [ $added = 0 ] && grep -q '^#' <<<"$line" && echo "$setup_text" >>"$bshtmp" && added=1
        done < "$install_to"
        mv -f "$bshtmp" "$install_to"
}   
if [ $CFG_NEW ]; then
   cd $THIS_DIR
   if [ $CFG_CUST ]; then
      cp bashrc_fe_custom.in bashrc_fe 
      cp cshrc_fe_custom.in cshrc_fe 
   else
      cp bashrc_fe.in bashrc_fe 
      cp cshrc_fe.in cshrc_fe 
   fi
   sed -i.bak s#CDSCFG_CFG#${CDSCFG_CFG}#g *hrc_fe
   if [ -e ~/.bashrc ]; then   
       if grep -q -i rtsetup ~/.bashrc ; then
           echo "rtsetup already added to bashrc"
       else
           echo "Add rtsetup to bashrc"
           insert_stdsetup bashrc_fe ~/.bashrc
       fi
   else
       echo "create .bashrc"
       cp bashrc_fe ~/.bashrc
   fi
   if [ -e ~/.cshrc ]; then 
       if grep -q -i rtsetup ~/.cshrc; then
           echo "rtsetup already added to cshrc"
       else
           echo "Add rtsetup to cshrc"
           insert_stdsetup cshrc_fe ~/.cshrc
       fi
   else
       echo "create .cshrc" 
       cp cshrc_fe ~/.cshrc
   fi
fi

